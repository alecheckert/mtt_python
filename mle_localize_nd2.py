'''
mle_localize_nd2.py -- use  maximum likelihood estimation 
(with initial guess provided by the radial symmetry method)
to localize spots in an ND2 tracking file.

For theory, see

Spot detection by generalized log-likelihood ratio test
	Arnauld, Nicolas, Hervé & Didier. "Dynamic multiple-target tracing to
	probe spatiotemporal cartography of cell membranes." Nature Methods
	5, 687 - 694 (2008)

Radial symmetry method for initial guess of particle position
	Parthasarathy, R. "Rapid, accurate particle tracking by
	calculation of radial symmetry centers." Nature Methods 9(7),
	724 - 726 (2012).

Maximum likelihood estimation of Gaussian spot with Poisson noise
	Smith CS, Joseph N, Rieger B & Lidke KA. "Fast, single-molecule
	localization that achieves theoretically minimum uncertainty."
	Nature Methods 7(5), 373 - 375 (2010).

Theoretical best-fitting Gaussian for a given NA and wavelength
	Zhang B, Josiane Z & Olivo-Marin JC. "Gaussian approximations
	of fluorescence microscope point-spread function models."
	Applied Optics 46(10), 1819 - 1829 (2007).

The log-likelihood maximization here is a modified Newton-Raphson
with damping and a dynamically adjusting ridge term to stabilize
the Hessian.

The routine works optimally when camera gain and bg are provided,
which can be estimated using the method of Ricardo Henriques.
However, it performs quite well on naive grayvalues as well.

'''
__author__ = 'Alec Heckert'
import click 
import numpy as np 
import pandas as pd
from scipy.special import erf 
from scipy.ndimage import uniform_filter
from scipy.stats import chi2
from scipy.optimize import minimize
import os
import sys
from nd2reader import ND2Reader 
import matplotlib.pyplot as plt 

@click.group()
def cli():
	'''
	Localize particles using maximum likelihood estimation under
	a Gaussian PSF model with Poisson noise. For usage info on
	a particle command, run

		python mle_localize_nd2.py <command_name> --help

	'''
	pass

@cli.command()
@click.argument('nd2_file', type = str)
@click.argument('frame_idx', type = int)
@click.option('-g', '--camera_gain', type = float, default = 110, help = 'default 110')
@click.option('-w', '--window_size', type = int, default = 9, help = 'default 9 pixels')
@click.option('-s', '--psf_scale', type = float, default = 1.45, help = 'default 1.45')
@click.option('-y', '--wavelength', type = float, default = 0.570, help = 'default 0.570 um')
@click.option('-n', '--na', type = float, default = 1.49, help = 'default 1.49')
@click.option('-p', '--pixel_size_um', type = float, default = 0.16, help = 'default 0.16 um/pixel')
@click.option('-e', '--error_rate', type = float, default = 7.0, help = 'default 7.0')
def test_localize(
	nd2_file,
	frame_idx,
	camera_gain = 110,
	window_size = 9,
	psf_scale = 1.45,
	wavelength = 0.570,
	na = 1.49,
	pixel_size_um = 0.16,
	error_rate = 7.0,
):
	nd2_obj = ND2Reader(nd2_file)
	frame = nd2_obj.get_frame_2D(t = frame_idx)
	frame = frame.astype('float64') / camera_gain
	localizations = detect_and_localize(
		frame,
		window_size = window_size,
		psf_scale = psf_scale,
		wavelength = wavelength,
		na = na,
		pixel_size_um = pixel_size_um,
		error_rate = error_rate
	)
	n_locs = localizations.shape[0]

	fig, ax = plt.subplots(1, 2, figsize = (6, 3))
	for i in range(2): ax[i].imshow(frame, cmap = 'gray')
	for loc_idx in range(n_locs):
		ax[1].plot(
			[localizations[loc_idx, 5] - 0.5],
			[localizations[loc_idx, 4] - 0.5],
			marker = '.',
			markersize = 4,
			color = 'r',
		)
	plt.show(); plt.close()

	half_w = int(window_size // 2)
	n_sides = int(np.ceil(np.sqrt(n_locs)))
	fig, ax = plt.subplots(n_sides, 2 * n_sides, figsize = (8, 4))
	for loc_idx in range(n_locs):
		y_ax = loc_idx // n_sides 
		x_ax = loc_idx % n_sides 
		y_detect, x_detect = localizations[loc_idx, :2].astype('uint16')
		sub_window = frame[y_detect - half_w : y_detect + half_w + 1, \
			x_detect - half_w : x_detect + half_w + 1]
		for j in range(2):
			ax[y_ax, 2 * x_ax + j].imshow(sub_window, cmap = 'gray')
		ax[y_ax, 2 * x_ax + 1].plot(
			[localizations[loc_idx, 5] - 0.5 - x_detect + half_w],
			[localizations[loc_idx, 4] - 0.5 - y_detect + half_w],
			marker = '.',
			markersize = 10,
			color = 'r',
		)
		for j in range(2):
			ax[y_ax, 2 * x_ax + j].set_xticks([])
			ax[y_ax, 2 * x_ax + j].set_yticks([])
	plt.show(); plt.close()

	return localizations

@cli.command()
@click.argument('nd2_file', type = str)
@click.option('-bg', '--camera_bg', type = float, default = 0, help = 'default 0')
@click.option('-g', '--camera_gain', type = float, default = 110, help = 'default 110')
@click.option('-w', '--window_size', type = int, default = 9, help = 'default 9 pixels')
@click.option('-s', '--psf_scale', type = float, default = 1.45, help = 'default 1.45')
@click.option('-y', '--wavelength', type = float, default = 0.570, help = 'default 0.570 um')
@click.option('-n', '--na', type = float, default = 1.49, help = 'default 1.49')
@click.option('-p', '--pixel_size_um', type = float, default = 0.16, help = 'default 0.16 um')
@click.option('-e', '--error_rate', type = float, default = 7.0, help = 'default 7.0')
@click.option('-m', '--max_locs', type = int, default = 1000000, help = 'default 1000000')
def localize(
	nd2_file,
	camera_bg = 0,
	camera_gain = 110,
	window_size = 9,
	psf_scale = 1.45,
	wavelength = 0.664,
	na = 1.49,
	pixel_size_um = 0.16,
	error_rate = 7.0,
	max_locs = 1000000
):
	if os.path.isdir(nd2_file):
		nd2_files = ['%s/%s' % (nd2_file, j) for j in os.listdir(nd2_file) if '.nd2' in j]
	elif os.path.isfile(nd2_file):
		nd2_files = [nd2_file]
	else:
		print('File/directory %s not found' % nd2_file)
		exit(1)

	out_txts = [j.replace('.nd2', '_mle_locs.txt') for j in nd2_files]
	for nd2_idx, nd2_file in enumerate(nd2_files):
		print('Localizing particles in file %s...' % nd2_file)
		out_txt = out_txts[nd2_idx]
		nd2_obj = ND2Reader(nd2_file)
		result = np.zeros((max_locs, 12), dtype = 'float')
		sigma = expected_gaussian_sigma(
			wavelength = wavelength,
			na = na,
			pixel_size_um = pixel_size_um,
			psf_scale = psf_scale
		)
		half_w = window_size // 2
		particle_idx = 0
		frame_idx = 0

		while particle_idx < max_locs:
			try:
				image = (nd2_obj.get_frame_2D(t = frame_idx).astype('float64') \
					- camera_bg) / camera_gain

				# Occasionally Nikon will record empty frames; ignore these
				if len(image.shape) != 2:
					frame_idx += 1
					continue 
					
				detections = detect(
					image,
					sigma,
					window_size = window_size,
					error_rate = error_rate,
				)
				n_detect = detections.shape[0]

				result[particle_idx : particle_idx + n_detect, 1] = frame_idx 
				result[particle_idx : particle_idx + n_detect, 4] = detections[:, 2]
				result[particle_idx : particle_idx + n_detect, 5] = detections[:, 3]
				result[particle_idx : particle_idx + n_detect, 7:9] = detections[:, :2]

				for detect_idx in range(n_detect):
					detect_y, detect_x = detections[detect_idx, :2].astype('uint16')
					sub_window = image[
						detect_y - half_w : detect_y + half_w + 1,
						detect_x - half_w : detect_x + half_w + 1
					]
					if (sub_window.shape[0] == window_size) and (sub_window.shape[1] == window_size):
						fit_pars = mle(sub_window, sigma)
						fit_pars[0] += detections[detect_idx, 0] - half_w
						fit_pars[1] += detections[detect_idx, 1] - half_w 
						result[particle_idx, 2:4] = fit_pars[:2]
						result[particle_idx, 10:] = fit_pars[2:]
					else:
						result[particle_idx, 6] = 1
					result[particle_idx, 9] = sub_window.var()
					particle_idx += 1

				frame_idx += 1
				sys.stdout.write('Finished with %d frames...\r' % frame_idx)
				sys.stdout.flush()
			except KeyError:   #EOF
				break 

		print('\nFormatting result...')
		result = result[:particle_idx, :]
		result = result[~result[:,6].astype('bool'), :]   #eliminate border detections
		result[:, 0] = np.arange(result.shape[0])         #particle_idx 

		result = pd.DataFrame(result, columns = [
			'particle_idx', 
			'frame_idx', 
			'y_coord_pixels', 
			'x_coord_pixels',
			'alpha', 
			'sig2', 
			'result_ok',
			'y_coord_pixels_detected',
			'x_coord_pixels_detected',
			'im_part_var',
			'mle_I0',
			'mle_bg',
		])
		print(result)
		result['particle_idx'] = result['particle_idx'].astype('uint16')
		result['frame_idx'] = result['frame_idx'].astype('uint16')
		result['result_ok'] = result['result_ok'].astype('bool')
		result.to_csv(out_txt, sep = '\t', index = False)
		print('Localized %d particles in file %s' % (len(result), nd2_file))	

def detect_and_localize(
	image,
	window_size = 9,
	psf_scale = 1.45,
	wavelength = 0.664,
	na = 1.49,
	pixel_size_um = 0.16,
	error_rate = 7.0
):
	'''
	result format:

		result[detection_idx, 0]	:	detected y-position (pixels, int)
		result[detection_idx, 1]	:	detected x-positoin (pixels, int)
		result[detection_idx, 2]	:	detected intensity
		result[detection_idx, 3]	:	detected sigma
		result[detection_idx, 4]	:	fitted y-position (pixels)
		result[detection_idx, 5]	:	fitted x-position (pixels)
		result[detection_idx, 6]	:	fitted spot intensity
		result[detection_idx, 7]	:	fitted bg intensity
		result[detection_idx, 8]	:	(unused)
		result[detection_idx, 9]	:	flag for border detections; 1s are filtered out

	'''
	sigma = expected_gaussian_sigma(
		wavelength = wavelength,
		na = na,
		pixel_size_um = pixel_size_um,
		psf_scale = psf_scale
	)
	detections = detect(
		image,
		sigma,
		window_size = window_size,
		error_rate = error_rate,
	)
	n_detect = detections.shape[0]
	result = np.zeros((n_detect, 10), dtype = 'float64')
	result[:, :4] = detections 
	half_w = int(window_size // 2)
	for detect_idx in range(n_detect):
		sub_window = image[
			int(detections[detect_idx, 0]) - half_w : \
				int(detections[detect_idx, 0]) + half_w + 1,
			int(detections[detect_idx, 1]) - half_w : \
				int(detections[detect_idx, 1]) + half_w + 1
		]
		if (sub_window.shape[0] == window_size) and (sub_window.shape[1] == window_size):
			fit_pars = mle(sub_window, sigma)
			fit_pars[0] += detections[detect_idx, 0] - half_w 
			fit_pars[1] += detections[detect_idx, 1] - half_w
			result[detect_idx, 4:8] = fit_pars
		else:
			result[detect_idx, 9] = 1
	result = result[~result[:, 9].astype('bool'), :]
	return result 

def detect(
	image,
	sigma,  #expected Gaussian sigma in pixels
	window_size = 9,
	error_rate = 7.0
):
	image = image.astype('double')
	N, M = image.shape
	pfa = chi2inv(error_rate)
	T = window_size ** 2
	hm = expand_window(
		np.ones((window_size, window_size)),
		N, M
	)
	tfhm = np.fft.fft2(hm)
	g = gaussian_model(sigma, window_size)
	gc = g - g.sum() / T
	Sgc2 = (gc ** 2).sum()
	hgc = expand_window(gc, N, M)
	tfhgc = np.fft.fft2(hgc)
	tfim = np.fft.fft2(image)
	m0 = np.real(np.fft.fftshift(np.fft.ifft2(tfhm * tfim))) / T
	tfim2 = np.fft.fft2(image ** 2)
	Sim2 = np.real(np.fft.fftshift(np.fft.ifft2(tfhm * tfim2)))
	T_sig0_2 = Sim2 - T * (m0 ** 2)
	alpha = np.real(np.fft.fftshift(np.fft.ifft2(tfhgc * tfim))) / Sgc2
	test = 1 - (Sgc2 * (alpha ** 2)) / T_sig0_2
	test = (test > 0) * test + (test <= 0)
	peaks = -T * np.log(test)
	peaks[np.isnan(peaks)] = 0
	detections = peaks > pfa
	detect_pfa = local_max_2d(peaks).astype('bool') & detections
	detected_positions = np.asarray(np.nonzero(detect_pfa)).T
	n_detect = detected_positions.shape[0]
	
	# Adjust for the off-by-1 error
	detected_positions += 1

	sig1_2 = (T_sig0_2 - (alpha ** 2) * Sgc2) / T 
	result = np.zeros((detected_positions.shape[0], 4), dtype = 'double')
	result[:, :2] = detected_positions
	result[:, 2] = alpha[detected_positions[:, 0], detected_positions[:, 1]]
	result[:, 3] = sig1_2[detected_positions[:, 0], detected_positions[:, 1]]

	return result  #0: y_pos, 1: x_pos, 2: alpha, 3: sig2
	
def mle(
	image, 
	sigma,
	convergence = 1.0e-3,
	max_iter = 50,
	damp = 0.2,
	min_hessian_det = 0.01, 
):
	# Instantiate necessary variables in 64-bit depth
	image = image.astype('float64')
	y_field, x_field = np.mgrid[:image.shape[0], :image.shape[1]]
	y_profile = image.sum(axis = 1)
	x_profile = image.sum(axis = 0)
	sqrt_2_s2 = np.sqrt(2 * (sigma**2))

	u_k = np.zeros(image.shape, dtype = 'float64')
	u_k_2 = np.zeros(image.shape, dtype = 'float64')
	E_y = np.zeros(image.shape, dtype = 'float64')
	E_x = np.zeros(image.shape, dtype = 'float64')
	dEy_dy = np.zeros(image.shape, dtype = 'float64')
	dEx_dx = np.zeros(image.shape, dtype = 'float64')
	ddEx_ddx = np.zeros(image.shape, dtype = 'float64')
	ddEy_ddy = np.zeros(image.shape, dtype = 'float64')
	aux_0 = np.zeros(image.shape, dtype = 'float64')
	aux_1 = np.zeros(image.shape, dtype = 'float64')
	aux_2 = np.zeros(image.shape, dtype = 'float64')

	pars = np.zeros(4, dtype = 'float64')
	grad = np.ones(4, dtype = 'float64')
	update = np.ones(4, dtype = 'float64')
	hessian = np.zeros((4, 4), dtype = 'float64')
	
	# Use the radial symmetry method to find an initial guess for (y, x)
	y_rs, x_rs = radial_symmetry(image)

	# Determine initial guesses for amplitude and background terms. 
	#  The background term is set to the mean of the outer ring of
	#  pixels, while the amplitude term is solved from the radial
	#  symmetry estimates of (y, x) and the solution of the resulting
	#  LL model for the highest-intensity pixel.
	b = np.array([
		image[0,:].mean(),
		image[-1,:].mean(),
		image[:,0].mean(),
		image[:,-1].mean()
	]).mean()
	max_idx = np.argmax(image)
	y_max_idx = max_idx // image.shape[1]
	x_max_idx = max_idx % image.shape[1]
	E_y_max = 0.5 * (erf((y_max_idx + 0.5 - y_rs) / sqrt_2_s2) \
		- erf((y_max_idx - 0.5 - y_rs) / sqrt_2_s2))
	E_x_max = 0.5 * (erf((x_max_idx + 0.5 - x_rs) / sqrt_2_s2) \
		- erf((x_max_idx - 0.5 - x_rs) / sqrt_2_s2))
	I0 = (image.max() - b) / (E_y_max * E_x_max)
	pars[0] = y_rs
	pars[1] = x_rs
	pars[2] = I0
	pars[3] = b 

	n_iter = 0
	while (n_iter < max_iter) and (np.abs(update) > convergence).any():
		E_y[:] = 0.5 * (erf((y_field + 0.5 - pars[0]) / sqrt_2_s2) - \
			erf((y_field - 0.5 - pars[0]) / sqrt_2_s2))
		E_x[:] = 0.5 * (erf((x_field + 0.5 - pars[1]) / sqrt_2_s2) - \
			erf((x_field - 0.5 - pars[1]) / sqrt_2_s2))
		u_k[:] = pars[2] * E_y * E_x + pars[3]
		u_k_2[:] = u_k ** 2
		aux_0[:] = image / u_k - 1
		aux_1[:] = image / u_k_2
		aux_2[:] = image * pars[3] / u_k_2 - 1

		dEy_dy[:] = ( 
			np.exp(-(((y_field - 0.5 - pars[0])**2) / (2 * sigma**2))) \
			- np.exp(-(((y_field + 0.5 - pars[0])**2) / (2 * sigma**2)))
		) / (sigma * np.sqrt(2 * np.pi))
		dEx_dx[:] = ( 
			np.exp(-(((x_field - 0.5 - pars[1])**2) / (2 * sigma**2))) \
			- np.exp(-(((x_field + 0.5 - pars[1])**2) / (2 * sigma**2)))
		) / (sigma * np.sqrt(2 * np.pi))
		ddEy_ddy[:] = (
			(y_field - 0.5 - pars[0]) * np.exp(-(((y_field - 0.5 - pars[0])**2) / (2 * sigma**2))) \
			- (y_field + 0.5 - pars[0]) * np.exp(-(((y_field + 0.5 - pars[0])**2) / (2 * sigma**2)))
		) / ((sigma**3) * np.sqrt(2 * np.pi))
		ddEx_ddx[:] = (
			(x_field - 0.5 - pars[1]) * np.exp(-(((x_field - 0.5 - pars[1])**2) / (2 * sigma**2))) \
			- (x_field + 0.5 - pars[1]) * np.exp(-(((x_field + 0.5 - pars[1])**2) / (2 * sigma**2)))
		) / ((sigma**3) * np.sqrt(2 * np.pi))

		grad[0] = (pars[2] * E_x * dEy_dy * aux_0).sum()
		grad[1] = (pars[2] * E_y * dEx_dx * aux_0).sum()
		grad[2] = (E_x * E_y * aux_0).sum()
		grad[3] = aux_0.sum()

		hessian[0,0] = (
			pars[2] * E_x * ddEy_ddy * aux_0 - aux_1 * (pars[2] * E_x * dEy_dy)**2
		).sum()
		hessian[0,1] = -pars[2] * (
			dEy_dy * dEx_dx * aux_2
		).sum()
		hessian[0,2] = (
			E_x * dEy_dy * aux_2
		).sum()
		hessian[0,3] = -pars[2] * (
			aux_1 * E_x * dEy_dy
		).sum()
		hessian[1,0] = hessian[0,1]
		hessian[1,1] = (
			pars[2] * E_y * ddEx_ddx * aux_0 - aux_1 * (pars[2] * E_y * dEx_dx)**2
		).sum()
		hessian[1,2] = (
			E_x * dEx_dx * aux_2
		).sum()
		hessian[1,3] = - pars[2] * (
			aux_1 * E_y * dEx_dx
		).sum()
		hessian[2,0] = hessian[0,2]
		hessian[2,1] = hessian[1,2]
		hessian[2,2] = - ((
			image * (pars[2] / u_k - 1)**2
		) / (pars[2]**2)).sum()
		hessian[2,3] = - (
			aux_1 * E_x * E_y
		).sum()
		hessian[3,0] = hessian[0,3]
		hessian[3,1] = hessian[1,3]
		hessian[3,2] = hessian[2,3]
		hessian[3,3] = - aux_1.sum()

		# Invert the Hessian matrix. To do this, first check
		#   the determinant for stability and add a ridge term
		#   if necessary (the ridge term is equivalent to gradient
		#   ascent). Continue to increase the ridge term if the
		#   matrix proves difficult to invert.
		while np.abs(np.linalg.det(hessian)) <= min_hessian_det:
			hessian -= np.identity(4, dtype = 'float64')
		switch = True
		while switch:
			try:
				hessian_inv = np.linalg.inv(hessian)
				switch = False
			except np.linalg.linalg.LinAlgError:
				hessian -= np.identity(4, dtype = 'float64')

		update = damp * hessian_inv.dot(grad)

		#The magnitude of the I0 update is often low
		# compared to the other parameters, so multiply it by 1000
		update[2] *= 1e4

		pars = pars - update
		n_iter += 1

		# If an ill-conditioned problem is detected (usually arising
		# from two localizations too close together), revert to Gauss-
		# Newton which is greedier and fits the closest one
		if n_iter >= max_iter:
			gn_result = gauss_newton(
				image,
				sigma,
				window_size = image.shape[0],
				ITER_MAX = 50,
			)
			return np.array([gn_result[0], gn_result[1], gn_result[2], gn_result[4]])

	# If the MLE is too close to the edge (a sign of divergence),
	#   default to the radial symmetry guess
	if (pars[:2] < 3).any() or (pars[:2] > (image.shape[0] - 3)).any():
		pars[0] = y_rs
		pars[1] = x_rs
		pars[2] = I0
		pars[3] = b
		u_k = I0 * 0.25 * (erf((y_field + 0.5 - y_rs) / sqrt_2_s2) \
			- erf((y_field - 0.5 - y_rs) / sqrt_2_s2)) \
			* (erf((x_field + 0.5 - x_rs) / sqrt_2_s2) \
			- erf((x_field - 0.5 - x_rs) / sqrt_2_s2)) + b
	factor = 0.477
	pars[0] += factor
	pars[1] += factor

	return pars

def radial_symmetry(
	image,
):
	Ny, Nx = image.shape
	Ny_half = int(Ny // 2)
	Nx_half = int(Nx // 2)
	ym_one_col = np.arange(Ny - 1) - Ny_half + 0.5
	xm_one_row = np.arange(Nx - 1) - Nx_half + 0.5
	ym = np.outer(ym_one_col, np.ones(Ny - 1))
	xm = np.outer(np.ones(Nx - 1), xm_one_row)

	dI_du = image[:Ny-1, 1:] - image[1:, :Nx-1]
	dI_dv = image[:Ny-1, :Nx-1] - image[1:, 1:]

	fdu = uniform_filter(dI_du, 3)
	fdv = uniform_filter(dI_dv, 3)

	dI2 = (fdu ** 2) + (fdv ** 2)
	m = -(fdv + fdu) / (fdu - fdv)
	m[np.isinf(m)] = 9e9

	b = ym - m * xm

	sdI2 = dI2.sum()
	ycentroid = (dI2 * ym).sum() / sdI2
	xcentroid = (dI2 * xm).sum() / sdI2
	w = dI2 / np.sqrt((xm - xcentroid)**2 + (ym - ycentroid)**2)

	# Correct nan / inf values
	w[np.isnan(m)] = 0
	b[np.isnan(m)] = 0
	m[np.isnan(m)] = 0

	# Least-squares analytical solution (equiv. to lsradialcenterfit)
	wm2p1 = w / ((m**2) + 1)
	sw = wm2p1.sum()
	smmw = ((m**2) * wm2p1).sum()
	smw = (m * wm2p1).sum()
	smbw = (m * b * wm2p1).sum()
	sbw = (b * wm2p1).sum()
	det = (smw ** 2) - (smmw * sw)
	xc = (smbw*sw - smw*sbw)/det
	yc = (smbw*smw - smmw*sbw)/det

	# Adjustment of coordinates
	yc = (yc + (Ny + 1) / 2.0) - 1
	xc = (xc + (Nx + 1) / 2.0) - 1

	# Add 0.5 pixel shift to get back to the original indexing, or not
	# fit_vector = np.array([yc, xc]) + 0.5
	fit_vector = np.array([yc, xc])

	return fit_vector 

def gauss_newton(
	im_part,
	psf_std,
	window_size = 9,
	ITER_MAX = 50,
):
	half_window = window_size // 2
	prec_rel = 0.01
	wn_i, wn_j = im_part.shape
	N = wn_i * wn_j
	refi = 0.5 + np.arange(wn_i) - wn_i / 2
	refj = 0.5 + np.arange(wn_j) - wn_j / 2

	def gn_iter(
		r0,
		i0,
		j0,
		x, #the image
		sig2init,
		p_dr,
		p_di,
		p_dj,
		current_iter = 0,
		max_iter = 1000,
	):
		pp_r = r0 - p_dr
		pp_i = i0 - p_di
		pp_j = j0 - p_dj

		again = 1
		while again:
			i = refi - i0
			j = refj - j0
			ii = np.outer(i, np.ones(wn_j))
			jj = np.outer(np.ones(wn_i), j)
			iiii = (ii ** 2).astype('float64')
			jjjj = (jj ** 2).astype('float64')
			g = np.exp(-(1/(2 * r0**2)) * (iiii + jjjj)) / (np.sqrt(np.pi) * r0)
			gc = (g - g.sum() / N).astype('float64')
			Sgc2 = (gc**2).sum()
			g_div_sq_r0 = g / (r0**2)
			if Sgc2 != 0.0:
				alpha = (x * gc).astype('float64').sum() / Sgc2
			else:
				alpha = 0
			x_alphag = x - alpha * g
			m = x_alphag.sum() / N
			err = x_alphag - m
			sig2 = (err**2).sum() / N
			current_iter += 1
			if (sig2 > sig2init):
				p_di = p_di / 10.0
				p_dj = p_dj / 10.0
				i0 = pp_i + p_di
				j0 = pp_j + p_dj
				current_iter += 1
				if (max([abs(p_dr), abs(p_di), abs(p_dj)]) > prec_rel):
					n_r = r0
					n_i = i0  
					n_j = j0 
					dr = 0
					di = 0
					dj = 0
					return n_r, n_i, n_j, dr, di, dj, alpha, sig2, m 
			else:
				again = 0
			current_iter += 1
			if current_iter > max_iter:
				again = 0

		d_g_i0 = ii * g_div_sq_r0
		d_g_j0 = jj * g_div_sq_r0
		dd_g_i0 = (-1 + iiii/(r0**2)) * g_div_sq_r0
		dd_g_j0 = (-1 + jjjj/(r0**2)) * g_div_sq_r0

		d_J_i0 = alpha * (d_g_i0 * err).sum()
		d_J_j0 = alpha * (d_g_j0 * err).sum()

		dd_J_i0 = alpha * (dd_g_i0 * err).sum() - (alpha**2) * (dd_g_i0**2).sum()
		dd_J_j0 = alpha * (dd_g_j0 * err).sum() - (alpha**2) * (dd_g_j0**2).sum()

		dr = 0
		n_r = r0 
		di = -d_J_i0 / dd_J_i0
		dj = -d_J_j0 / dd_J_j0
		n_i = i0 + di
		n_j = j0 + dj

		return n_r, n_i, n_j, dr, di, dj, alpha, sig2, m 

	localization_flags = [-1.5, 1.5, -1.5, 1.5, psf_std - 50 * psf_std / 100, \
		psf_std + 50 * psf_std / 100]
	test = True 
	r = psf_std
	i = 0.0
	j = 0.0
	dr = 1
	di = 1
	dj = 1
	fin = 0.01
	sig2 = np.inf 
	cpt = 0

	while test:
		r, i, j, dr, di, dj, alpha, sig2, offset = \
			gn_iter(r, i, j, im_part, sig2, dr, di, dj)
		cpt += 1
		test = max([abs(di), abs(dj)]) > fin 
		if cpt > ITER_MAX:
			test = False
		result_ok = not ((i < localization_flags[0]) or (i > localization_flags[1]) \
			or (j < localization_flags[2]) or (j > localization_flags[3]) or \
			(r < localization_flags[4]) or (r > localization_flags[5]))
		test = test and result_ok

	return (
		i + half_window + 0.5, #y-coordinate, adjusted to match MLE
		j + half_window + 0.5, #x-coordinate, adjusted to match MLE
		alpha,  #mean amplitude
		sig2,   #noise power
		offset, #background level
		r,      #r0
		result_ok,
		im_part.var()
	)

def expand_window(image, N, M):
	N_in, M_in = image.shape
	out = np.zeros((N, M))
	nc = np.floor(N/2 - N_in/2).astype(int)
	mc = np.floor(M/2 - M_in/2).astype(int)
	out[nc:nc+N_in, mc:mc+M_in] = image
	return out

def local_max_2d(image):
	N, M = image.shape
	ref = image[1:N-1, 1:M-1]
	pos_max_h = (image[0:N-2, 1:M-1] < ref) & (image[2:N, 1:M-1] < ref)
	pos_max_v = (image[1:N-1, 0:M-2] < ref) & (image[1:N-1, 2:M] < ref)
	pos_max_135 = (image[0:N-2, 0:M-2] < ref) & (image[2:N, 2:M] < ref)
	pos_max_45 = (image[2:N, 0:M-2] < ref) & (image[0:N-2, 2:M] < ref)
	peaks = np.zeros((N, M))
	peaks[1:N-1, 1:M-1] = pos_max_h & pos_max_v & pos_max_135 & pos_max_45
	peaks = peaks * image
	return peaks

def expected_gaussian_sigma(
	wavelength = 0.570,
	na = 1.49,
	pixel_size_um = 0.16,
	psf_scale = 1.35,
):
	'''
	Based on Zhang 2007's estimate.
	'''
	return psf_scale * 0.55 * wavelength / (na * 1.17 * pixel_size_um * 2)

def chi2inv(log_likelihood_threshold):
	def min_function(X):
		return (log_likelihood_threshold + np.log10(1-chi2.cdf(X,1)))**2
	return minimize(min_function, 20.0).x[0]

def gaussian_model(psf_std, window_size):
	refi = (0.5 + np.arange(0, window_size) - window_size / 2).astype('int8')
	ii = np.outer(refi, np.ones(window_size, dtype = 'int8'))
	jj = np.outer(np.ones(window_size, dtype = 'int8'), refi)
	g = np.exp(-((ii**2) + (jj**2)) / (2 * (psf_std**2))) / (np.sqrt(np.pi) * psf_std)
	return g

def get_n_frames(nd2_obj):
	n_frames = 0
	while 1:
		try:
			frame = nd2_obj.get_frame_2D(t = n_frames)
			n_frames += 1
		except (ValueError, KeyError) as e2:
			return n_frames 

if __name__ == '__main__':
	cli()
