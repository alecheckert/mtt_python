'''
track.py -- updated 190829

'''
import click 
import numpy as np 
import pandas as pd 
from scipy import io as sio 
from munkres import Munkres 
import os
import random 
import sys
from copy import copy
from time import time
m = Munkres()

@click.command()
@click.argument('localization_csv', type = str)
@click.argument('d_max', type = float)
@click.argument('d_bound_naive', type = float)
@click.option('-o', '--out_mat_prefix', type = str, default = None, help = 'default None')
@click.option('-s', '--search_exp_fac', type = float, default = 1, help = 'default 1')
@click.option('-d', '--sep', type = str, default = '\t', help = 'default tab')
@click.option('-p', '--pixel_size_um', type = float, default = 0.16, help = 'default 0.16 um per pixel')
@click.option('-f', '--frame_interval_sec', type = float, default = 0.00748, help = 'default 0.00748 seconds')
@click.option('-m', '--min_int', type = float, default = 0.0, help = 'default 0.0')
@click.option('-b', '--max_blinks', type = int, default = 2, help = 'default 2')
@click.option('-w', '--window_size', type = int, default = 9, help = 'default 9')
@click.option('-k', '--k_return_from_blink', type = float, default = 1.0, help = 'default 1.0')
@click.option('-yi', '--y_int', type = float, default = 0.5, help = 'default 0.5')
@click.option('-yd', '--y_diff', type = float, default = 0.9, help = 'default 0.9')
def track_files(
	localization_csv,
	d_max,
	d_bound_naive,
	out_mat_prefix = None, 
	search_exp_fac = 1,
	sep = '\t',
	pixel_size_um = 0.16,
	frame_interval_sec = 0.00748,
	min_int = 0.0,  # to start new trajectory
	max_blinks = 2,
	window_size = 9,  # for detection
	k_return_from_blink = 1.0,  # Poisson rate; frames^-1
	y_int = 0.5,
	y_diff = 0.9,
):
	'''
	Reconnect a set of single molecule localizations
	into trajectories.

	INPUT

	localization_csv (str): a *locs.txt file with raw
	localizations. See file specification below.

	If given a directory and not an individual file,
	track.py will execute on each *locs.txt file
	in that directory.

	d_max (float): maximum expected diffusion coefficient
	in um^2 s^-1 (for instance: 20.0)

	d_bound_naive (float): guess for the diffusion coefficient
	of the bound fraction in um^2 s^-1 (for instance: 0.1)

	out_mat_prefix (str): prefix for *Tracked.mat output
	files. 

	sep (char): column delimiter in *localization_csv*

	pixel_size_um (float): um per pixel

	frame_interval_sec (float): the frame interval in 
	seconds

	min_int (float): fitted intensity threshold for new
	localizations to start their own trajectories

	max_blinks (int): maximum number of frames tolerated
	before a blinking trajectory is terminated

	window_size (int): size of the window used for the
	detection test in pixels

	k_return_from_blink (float): penalty for reconecting
	trajectories in blink

	y_int (float, between 0.0 and 1.0): weights used in
	the intensity test for tracking

	y_diff (float, between 0.0 and 1.0): relative contribution
	of the ``bound`` diffusion coefficient to tracking

	*localization_csv* encodes a dataframe with rows indexed
	to individual localizations and columns with the following
	specification:

		df[loc_idx, 0]	:	global loc index

		df[loc_idx, 1]	:	frame index

		df[loc_idx, 2]	: 	fitted y position (pixels)

		df[loc_idx, 3]	:	fitted x position (pixels)

		df[loc_idx, 4]	:	intensity from detection test

		df[loc_idx, 5]	:	ml variance of detection window (H1)

		df[loc_idx, 6]	:	flag for border detections

		df[loc_idx, 7]	:	detected y position (pixels)

		df[loc_idx, 8]	:	detected x position (pixels)

		df[loc_idx, 9]	:	ml variance of detection window (H0)

	OUTPUT

	*Tracked.mat files, each corresponding to one of the
	localization CSVs. These encode a dataframe A with
	the following indexing scheme:

		A[trajectory_index, 0] = positions (um)
		A[trajectory_index, 1] = frames
		A[trajectory_index, 2] = timestamps (seconds)

	'''
	if os.path.isdir(localization_csv):
		file_list = ['%s/%s' % (localization_csv, i) for i in os.listdir(localization_csv) if 'locs.txt' in i]
	elif os.path.isfile(localization_csv) and 'locs.txt' in localization_csv:
		file_list = [localization_csv]
	else:
		print('Input %s not recognized. Must be a *locs.txt or directory containing them.' % localization_csv)
		exit(1)

	for file_name in file_list:
		if out_mat_prefix != None:
			out_mat_file = '%s_%sTracked.mat' % (out_mat_prefix, file_name.replace('locs.txt', ''))
		else:
			out_mat_file = '%sTracked.mat' % file_name.replace('locs.txt', '')
		trajectories = track( 
			file_name,
			d_max,
			d_bound_naive,
			out_mat_file = out_mat_file,
			search_exp_fac = search_exp_fac,
			sep = sep,
			pixel_size_um = pixel_size_um,
			frame_interval_sec = frame_interval_sec,
			min_int = min_int,
			max_blinks = max_blinks,
			window_size = window_size,
			k_return_from_blink = k_return_from_blink,
			y_int = y_int,
			y_diff = y_diff,
		)
		print('Finished writing %s' % out_mat_file)


def save_trajectories_to_mat(
	trajectories,
	mat_file_name,
	frame_interval_sec,
	pixel_size_um = 0.16
):
	result_list = []
	for traj_idx in range(len(trajectories)):
		trajectories[traj_idx].positions *= pixel_size_um 
		frames = trajectories[traj_idx].frames 
		times = [i * frame_interval_sec for i in frames]
		result_list.append([trajectories[traj_idx].positions, frames, times])
	result = {'trackedPar' : result_list}
	sio.savemat(mat_file_name, result)
	return result 

def track(
	localization_csv,
	d_max,
	d_bound_naive,
	out_mat_file = None, 
	search_exp_fac = 1,
	sep = '\t',
	pixel_size_um = 0.16,
	frame_interval_sec = 0.00748,
	min_int = 0.0,  # to start new trajectory
	max_blinks = 2,
	window_size = 9,  # for detection
	k_return_from_blink = 1.0,  # Poisson rate; frames^-1
	y_int = 0.5,
	y_diff = 0.9,
):
	'''
	INPUT
		localization_csv	:	str, CSV encoding a dataframe of
								the type detailed belowed
		d_max				:	float, maximum expected diffusion
								coefficient for free fraction in
								um^2 s^-1
		d_bound_naive		:	float, the naive estimate for the
								bound diffusion coefficient
		out_mat_file		:	str, output *Tracked.mat file
								if desired
		search_exp_fac		:	float, factor by which to expand
								the search radius for reconnection,
								if desired
		sep					:	char, column delimiter in 
								*localization_csv*
		pixel_size_um		:	float, um per pixel
		frame_interval_sec	:	float, interval between frames
		min_int				:	float, the minimum estimated 
								intensity for a localization to 
								start a new trajectory
		max_blinks			:	int, the maximum number of blinks
								tolerated before a trajectory is
								terminated
		window_size			:	int, window edge length for the
								detection test
		k_return_from_blink	:	float, the penalty for reconnecting
								blinking trajectories
		y_int				:	float,
		y_diff				:	float, weight toward `bound` fraction


	Format of the dataframe *df* encoded in localization_csv:
		df[loc_idx, 0]		:	global_loc_idx
		df[loc_idx, 1]		:	frame_idx
		df[loc_idx, 2]		:	fitted y-position (float)
		df[loc_idx, 3]		:	fitted x-position (float)
		df[loc_idx, 4]		:	intensity from detection H1 (`alpha`)
		df[loc_idx, 5]		:	noise variance under detection H1 (`sig2`)
		df[loc_idx, 6]		:	flag for bad results (used in border test);
								should be False/0 for all
		df[loc_idx, 7]		:	detected y-position (int)
		df[loc_idx, 8]		:	detected x-position (int)
		df[loc_idx, 9]		:	noise variance under detection H0 
								(equivalent to sample variance of the
								detection window)
		df[loc_idx, 10]		:	fitted I0 (loc method-dependent)
		df[loc_idx, 11[		:	fitted I_bg (loc method-dependent)

	RETURNS
		list of Trajectory() objects, each with attribute positions,
			n_blinks, and frames

	'''
	sig2_free = 2.0 * d_max * frame_interval_sec / (pixel_size_um ** 2)
	sig2_bound_naive = 2.0 * d_bound_naive * frame_interval_sec / (pixel_size_um ** 2)
	search_radius = search_exp_fac * np.sqrt(np.pi * d_max * frame_interval_sec / (pixel_size_um ** 2))
	search_radius_sq = search_radius ** 2
	n_pixels = window_size ** 2

	locs = pd.read_csv(localization_csv, sep = sep)
	locs = np.asarray(locs)
	mean_spot_intensity = locs[:,4].mean()
	var_spot_intensity = locs[:,4].var()
	n_frames = locs[:,1].max()

	# Initialize the trajectories
	frame_idx = 0
	frame_locs = locs[locs[:,1] == frame_idx]
	active_trajectories = [
		Trajectory(
			positions = np.array([frame_locs[loc_idx, 2:4]]).astype('float64'),
			n_blinks = 0,
			frames = [frame_idx]
		) for loc_idx in range(frame_locs.shape[0])
	]

	# Iterate through the rest of the frames, saving
	# trajectories as you go
	completed_trajectories = []

	for frame_idx in range(1, n_frames + 1):
		frame_locs = locs[locs[:,1] == frame_idx].astype('float64')
		new_trajectories = []
	
		n_trajs = len(active_trajectories)
		n_locs = frame_locs.shape[0]

		# If no active trajectories, considering starting some from the locs
		if n_trajs == 0:
			for loc_idx in range(n_locs):
				if frame_locs[loc_idx, 4] >= min_int:
					new_trajectory = Trajectory(
						positions = np.array([frame_locs[loc_idx, 2:4]]),
						n_blinks = 0,
						frames = [frame_idx],
					)
					new_trajectories.append(new_trajectory)
			active_trajectories = copy(new_trajectories)
			continue 
		# If no localizations, set all of the trajectories into blink
		if n_locs == 0:
			for traj_idx in range(n_trajs):
				active_trajectories[traj_idx].n_blinks += 1
				if active_trajectories[traj_idx].n_blinks > max_blinks:
					completed_trajectories.append(copy(active_trajectories[traj_idx]))
				else:
					new_trajectories.append(copy(active_trajectories[traj_idx]))
			active_trajectories = copy(new_trajectories)
			continue 

		# Break the assignment problem into subproblems based on adjacency
		adjacency_matrix = np.zeros((n_trajs, n_locs), dtype = 'uint16')
		sq_radial_distances = compute_sq_radial_distance_array(
			np.asarray([traj.positions[-1, :] for traj in active_trajectories]),
			frame_locs[:, 2:4],
		)
		within_search_radius = (sq_radial_distances <= search_radius_sq).astype('uint16')
		subgraphs, y_index_lists, x_index_lists, trajs_without_locs, locs_without_trajs = \
			connected_components(within_search_radius)

		# Deal with trajectories without corresponding localizations
		for traj_idx in trajs_without_locs:
			active_trajectories[traj_idx].n_blinks += 1
			if active_trajectories[traj_idx].n_blinks > max_blinks:
				completed_trajectories.append(copy(active_trajectories[traj_idx]))
			else:
				new_trajectories.append(copy(active_trajectories[traj_idx]))

		# Deal with localizations without corresponding trajectories,
		#  starting new trajectories if necessary
		for loc_idx in locs_without_trajs:
			if frame_locs[loc_idx, 4] >= min_int:
				new_trajectory = Trajectory(
					positions = np.array([frame_locs[loc_idx, 2:4]]),
					n_blinks = 0,
					frames = [frame_idx],
				)
				new_trajectories.append(new_trajectory)

		# Deal with trajectory:localization reconnections
		for subgraph_idx, subgraph in enumerate(subgraphs):
			if subgraph.shape == (1, 1):
				traj_idx = y_index_lists[subgraph_idx][0]
				loc_idx = x_index_lists[subgraph_idx][0]
				new_positions = np.zeros((active_trajectories[traj_idx].positions.shape[0] \
					+ 1, 2), dtype = 'float64')
				new_positions[:-1, :] = active_trajectories[traj_idx].positions
				new_positions[-1, :] = frame_locs[loc_idx, 2:4]
				active_trajectories[traj_idx].positions = new_positions.copy()
				active_trajectories[traj_idx].frames.append(frame_idx)
				new_trajectories.append(copy(active_trajectories[traj_idx]))
			else:
				local_trajectories, finished_trajectories = assign(
					[active_trajectories[i] for i in y_index_lists[subgraph_idx]],
					frame_locs[x_index_lists[subgraph_idx], :],
					mean_spot_intensity,
					var_spot_intensity,
					sig2_bound_naive = sig2_bound_naive,
					sig2_free = sig2_free,
					n_pixels = n_pixels,
					k_return_from_blink = k_return_from_blink,
					y_int = y_int,
					y_diff = y_diff,
					max_blinks = max_blinks,
				)
				for trajectory in local_trajectories:
					new_trajectories.append(copy(trajectory))
				for trajectory in finished_trajectories:
					completed_trajectories.append(copy(trajectory))

		active_trajectories = copy(new_trajectories)

		sys.stdout.write('Tracked through %d/%d frames...\r' % (frame_idx, n_frames))
		sys.stdout.flush()

	print('')

	# Add any trajectories that are still running at the end
	for traj_idx in range(len(active_trajectories)):
		completed_trajectories.append(copy(active_trajectories[traj_idx]))

	# Save to file, if desired
	if out_mat_file != None:
		save_trajectories_to_mat(
			completed_trajectories,
			out_mat_file,
			frame_interval_sec,
			pixel_size_um = pixel_size_um,
		)

	return completed_trajectories

def connected_components(semigraph):
	y_indices = np.arange(semigraph.shape[0]).astype('uint16')
	x_indices = np.arange(semigraph.shape[1]).astype('uint16')

	where_y_without_x = (semigraph.sum(axis = 1) == 0)
	where_x_without_y = (semigraph.sum(axis = 0) == 0)
	y_without_x = y_indices[where_y_without_x]
	x_without_y = x_indices[where_x_without_y]
	semigraph = semigraph[~where_y_without_x, :]
	semigraph = semigraph[:, ~where_x_without_y]

	y_indices = y_indices[~where_y_without_x]
	x_indices = x_indices[~where_x_without_y]

	subgraphs = []
	subgraph_y_indices = []
	subgraph_x_indices = []

	unassigned_y, unassigned_x = (semigraph == 1).nonzero()
	current_idx = 2
	while len(unassigned_y) > 0:
		semigraph[unassigned_y[0], unassigned_x[0]] = current_idx
		prev_nodes = 0
		curr_nodes = 1
		while curr_nodes != prev_nodes:
			where_y, where_x = (semigraph == current_idx).nonzero()
			semigraph[where_y, :] *= current_idx
			semigraph[:, where_x] *= current_idx
			semigraph[semigraph > current_idx] = current_idx
			prev_nodes = curr_nodes
			curr_nodes = (semigraph == current_idx).sum()
		current_idx += 1 
		
		where_y = np.unique(where_y)
		where_x = np.unique(where_x)

		subgraph = semigraph[where_y, :]
		subgraph = subgraph[:, where_x]

		subgraphs.append(subgraph)
		subgraph_y_indices.append(y_indices[where_y])
		subgraph_x_indices.append(x_indices[where_x])
		
		unassigned_y, unassigned_x = (semigraph == 1).nonzero()

	return subgraphs, subgraph_y_indices, subgraph_x_indices, y_without_x, x_without_y

class Trajectory(object):
	def __init__(
		self,
		positions,
		n_blinks = 0,
		frames = [],
	):
		self.positions = positions
		self.n_blinks = n_blinks
		self.frames = frames

def assign(
	trajectories,
	localizations,
	mean_spot_intensity,
	var_spot_intensity,
	sig2_bound_naive = 0.0584,
	sig2_free = 8.77,
	n_pixels = 81,
	k_return_from_blink = 1.0,
	y_int = 0.5,
	y_diff = 0.9,
	max_blinks = 2,
):
	'''
	Given a set of trajectories and localizations in close
	proximity, reconnect the trajectories to the localizations
	to maximize the MTT log-likelihood function.

	INPUT
		trajectories			:	list of class Trajectory()
		localizations			:	np.array of shape (N_locs, 10),
									the localizations and their 
									associated information. See
									below for the required format.
		mean_spot_intensity		:	float, the mean estimated spot
									intensity across the whole 
									population of spots
		var_spot_intensity		:	float, the variance in estimated
									spot intensity across the whole
									population of spots
		sig2_bound_naive		:	float, naive estimate for
									2 D_{bound} dt. For instance,
									if D_{bound} = 0.1 um^2 s^-1,
									then sigma_bound_naive = 
									2 * D_{bound} * dt / (0.16**2)
									~= 0.0584
		sig2_free				:	float, the same parameter but
									for the free population
		n_pixels				:	int, the number of pixels
									per subwindow during the detection
									step
		k_return_from_blink		:	float, the rate constant governing
									the return-from-blink Poisson process
									in frames^-1
		y_int					:	float between 0.0 and 1.0
		y_diff					:	float between 0.0 and 1.0
								

	Format of localizations:

		localizations[loc_idx, 0] 	:	global loc idx [not used]
		localizations[loc_idx, 1]	:	frame idx [not used]
		localizations[loc_idx, 2]	:	fitted y position
		localizations[loc_idx, 3]	:	fitted x position
		localizations[loc_idx, 4]	:	detection intensity (alpha)
		localizations[loc_idx, 5]	:	detection noise variance (sig2)
		localizations[loc_idx, 6]	:	result_ok [not used]
		localizations[loc_idx, 7]	:	detected y position (int) [not used]
		localizations[loc_idx, 8]	:	detected x position (int) [not used]
		localizations[loc_idx, 9]	:	variance of subwindow for detection
										(equivalent to mle variance under
										detection hypothesis H0)
		localizations[loc_idx, 10]	:	fitted I0 [not used]
		localizations[loc_idx, 11]	:	fitted bg [not used]
		

	Note that localizations should be given as the slice of a larger
	dataframe that contains localizations that should be considered for
	detection. That is, it should probably correspond to a single frame_idx.
	But, you know, your funeral.

	'''
	n_trajs = len(trajectories)
	n_locs = localizations.shape[0]
	n_dim = n_trajs + n_locs

	#LL : the matrix of log-likelihoods.
	# LL[i,j] has different meanings, depending on whether the indices i
	# and j correspond to existing trajs/locs or to potential new trajectories
	# or trajectory terminations.

	# If i < n_trajs and j < n_locs, then LL[i,j] is the log likelihood
	#  that trajectory i connects to localization j.

	# If i >= n_trajs and j < n_locs, then LL[i,j] is the log likelihood
	#  that localization j starts a new trajectory.

	# If i < n_trajs and j >= n_locs, then LL[i,j] is the log likelihood
	#  that trajectory i enters blink (or ends, if it has reached max_blinks).

	# If i >= n_trajs and j >= n_locs, then LL[i,j] has no physical
	#  meaning and should be disregarded. These are essentially open indices
	#  to capture the possibility that the potential new trajectories do 
	#  not occur.
	LL = np.zeros((n_dim, n_dim), dtype = 'float64')

	# Putting a trajectory into blink costs a fixed log-likelihood
	LL[:, n_locs:] = -6

	# Starting a new trajectory is N_pixels * ( ln(sig2[model 1]) - ln(sig2[model 0]) ),
	# the log likelihood ratio of detection
	for loc_idx in range(n_locs):
		LL[n_trajs:, loc_idx] = n_pixels * (np.log(localizations[loc_idx, 5]) - \
			np.log(localizations[loc_idx, 9]))

	# For existing trajectories, compute the log-likelihood of reconnection
	#    to each new localization

	# First, compute the intensity law, which is the same for all trajectories.
	# Although the magnitudes are probably similar to the diffusion law, you may want to 
	# damp this anyway.
	P_int = np.zeros(localizations.shape[0], dtype = 'float64')
	below_mean_int = localizations[:, 4] < mean_spot_intensity
	P_int[below_mean_int] += (1 / mean_spot_intensity) * (1 - y_int)

	P_int += y_int * np.exp(
		-(localizations[:, 4] - mean_spot_intensity) / (2 * var_spot_intensity) \
	) / (np.sqrt(2 * np.pi * var_spot_intensity))
	L_int = np.log(P_int)
	
	for traj_idx, trajectory in enumerate(trajectories):
		# Return-from-blink penalty; usually -1 or -2 if k_return_from_blink == 1.0
		L_blink = -k_return_from_blink * trajectory.n_blinks + np.log(k_return_from_blink)

		# Displacement likelihood; varies from -5 to 0 and strongly favors closer reconnections
		squared_r = sq_radial_distance(trajectory.positions[-1, :], localizations[:, 2:4])
		r = np.sqrt(squared_r)
		
		# If there are enough past displacements, estimate the ``bound`` diffusion
		#  using the mean squared displacement. Else set to the default.
		n_disps = trajectory.positions.shape[0]
		if n_disps > 1:
			sig2_bound = 0.5 * (((trajectory.positions[1:, :] - \
				trajectory.positions[:-1, :])**2).sum(axis = 1) / \
					np.arange(1, n_disps)).mean() * (1 + trajectory.n_blinks)
		else:
			sig2_bound = sig2_bound_naive * (1 + trajectory.n_blinks)

		traj_sig2_free = sig2_free * (1 + trajectory.n_blinks)
		
		P_disp_bound = (r / sig2_bound) * np.exp(-squared_r / (2 * sig2_bound))
		P_disp_free = (r / traj_sig2_free) * np.exp(-squared_r / (2 * traj_sig2_free))
		L_diff = np.log(y_diff * P_disp_bound + (1 - y_diff) * P_disp_free)

		# Assign to the corresponding positions in the log-likelihood array
		LL[traj_idx, :n_locs] = L_blink + L_diff + L_int

	# The Hungarian algorithm will find the minimum path through its
	#  argument, so take the negative of the log-likelihoods.
	LL *= -1

	# Find the trajectory:localization assignment that maximizes the
	# total log likelihoods.
	assignments = m.compute(LL)
	assign_traj = [i[0] for i in assignments]
	assign_locs = [i[1] for i in assignments]

	# Deal with the aftermath, keeping track of which trajectories have
	#   finished and which are still running.
	active_trajectories = []
	finished_trajectories = []

	# For each trajectory, either set it into blink or match it
	#   with the corresponding localization
	for traj_idx, trajectory in enumerate(trajectories):
		if assign_locs[traj_idx] >= n_locs:  
			trajectory.n_blinks += 1
			if trajectory.n_blinks > max_blinks:
				finished_trajectories.append(trajectory)
			else:
				active_trajectories.append(trajectory)
		else:
			loc_idx = assign_locs[traj_idx]
			new_positions = np.zeros((trajectory.positions.shape[0] + 1, 2), dtype = 'float64')
			new_positions[:-1, :] = trajectory.positions
			new_positions[-1, :] = localizations[loc_idx, 2:4]
			trajectory.positions = new_positions
			trajectory.frames.append(int(localizations[loc_idx, 1]))
			active_trajectories.append(trajectory)

	# For each unpaired localization, start a new trajectory
	for traj_idx in range(n_trajs, n_dim):
		loc_idx = assign_locs[traj_idx]
		if loc_idx < n_locs:
			new_trajectory = Trajectory(
				positions =  np.array([localizations[loc_idx, 2 : 4]]),
				n_blinks = 0,
				frames = [int(localizations[loc_idx, 1])],
			)
			active_trajectories.append(new_trajectory)

	return active_trajectories, finished_trajectories
	
	
def euclidean_distance(vector, points):
	'''
	INPUT
		vector	:	np.array of shape (2)
		points	:	np.array of shape (n_points, 2)

	RETURNS
		np.array of shape (n_points), the distances
			from *vector* to each point in *points*

	'''
	return np.sqrt(((vector - points)**2).sum(axis = 1))

def sq_radial_distance(vector, points):
	return ((vector - points) ** 2).sum(axis = 1)

def compute_radial_distance_array(points_0, points_1):
	'''
	Slightly faster than doing it iteratively (less than an
	order of magnitude).

	INPUT
		points_0	:	np.array of shape (N, 2), coordinates
		points_1	:	np.array of shape (M, 2), coordinates

	RETURNS
		np.array of shape (N, M), the radial distances between
			each pair of points in the inputs

	'''
	array_points_0 = np.zeros((points_0.shape[0], points_1.shape[0], 2), dtype = 'float')
	array_points_1 = np.zeros((points_0.shape[0], points_1.shape[0], 2), dtype = 'float')
	for idx_0 in range(points_0.shape[0]):
		array_points_0[idx_0, :, :] = points_1 
	for idx_1 in range(points_1.shape[0]):
		array_points_1[:, idx_1, :] = points_0
	result = np.sqrt(((array_points_0 - array_points_1)**2).sum(axis = 2))
	return result 

def compute_sq_radial_distance_array(points_0, points_1):
	'''
	Similar, but for radial distances (avoids doing sqrt work).

	INPUT
		points_0	:	np.array of shape (N, 2), coordinates
		points_1	:	np.array of shape (M, 2), coordinates

	RETURNS
		np.array of shape (N, M), the radial distances between
			each pair of points in the inputs

	'''
	array_points_0 = np.zeros((points_0.shape[0], points_1.shape[0], 2), dtype = 'float')
	array_points_1 = np.zeros((points_0.shape[0], points_1.shape[0], 2), dtype = 'float')
	for idx_0 in range(points_0.shape[0]):
		array_points_0[idx_0, :, :] = points_1 
	for idx_1 in range(points_1.shape[0]):
		array_points_1[:, idx_1, :] = points_0
	result = ((array_points_0 - array_points_1)**2).sum(axis = 2)
	return result 

if __name__ == '__main__':
	track_files()
