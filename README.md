# mtt_python

Localization and trajectory reconstruction for live cell single molecule tracking, designed to work with Nikon ND2 files.

Requires the `nd2reader` and `munkres` packages.

For help, you can always use
```
python mle_localize_nd2.py --help
```

or 
```
python track.py --help
```

`mle_localize_nd2.py` is for the initial localization of spots in an ND2 movie, while `track.py` is for building trajectories from the localizations. Either function can be run on single files or on directories of files. `mle_localize_nd2.py` has two commands, one for localization and visualization of a single frame and another for full localization of an ND2 file. It is recommended to run the test localization before running the full localization:
```
	python mle_localize_nd2.py test_localize 
		nd2_file
		frame_index
		[-g camera_gain]
		[-w window_size]
		[-s psf_scale]
		[-y wavelength_in_um]
		[-n numerical_aperture]
		[-p pixel_size_um]
		[-e error_rate]
```

After making sure that the localization settings are working well for the ND2 file in question, the program can be run on the whole ND2 file (or a directory containing ND2 files) by passing
```
	python mle_localize_nd2.py localize
		nd2_file_or_directory_with_nd2_files
		[-g camera_gain]
		[-w window_size]
		[-s psf_scale]
		[-y wavelength_in_um]
		[-n numerical_aperture]
		[-p pixel_size_um]
		[-e error_rate]
		[-m max_locs]
```

The output of `mle_localize_nd2.py localize` are `locs.txt` files with a list of all of the localizations from the source ND2 file. These are named according to the source ND2 file. Do `python mle_localize.nd2 localize --help` for a file specification.

Finally, trajectory reconstruction can be run on `locs.txt` files using
```
	python track.py 
		localization_csv
		d_max
		d_bound_naive
		[-o out_mat_prefix]
		[-s search_exp_fac]
		[-d sep]
		[-p pixel_size_um]
		[-f frame_interval_seconds]
		[-m minimum_intensity_to_start_new_trajectories]
		[-b maximum_number_of_blinks_before_dropping_trajectory]
		[-w window_size]
		[-k penalty_to_return_from_blink]
		[-yi intensity_law_1_weight]
		[-yd diffusion_law_1_weight]
```

The output of `track.py` are `Tracked.mat` files with the trajectories. These can be loaded into Python with `scipy.io.loadmat`, and have the following format:
```
	from scipy import io as sio

	trajs = sio.loadmat('my_trajectories_Tracked.mat')['trackedPar']
	n_trajs = trajs.shape[0]
	for traj_idx in range(n_trajs):
		trajs[traj_idx][0]  # Positions in um
		trajs[traj_idx][1]  # Frames
		trajs[traj_idx][2]  # Timestamps for frames in seconds
```
	

### References ###

In addition to the materials provided in ``theory``, see the following source
papers:

Spot detection by generalized log-likelihood ratio test:
	Arnauld, Nicolas, Hervé & Didier. "Dynamic multiple-target tracing to
	probe spatiotemporal cartography of cell membranes." Nature Methods
	5, 687 - 694 (2008).

Radial symmetry method for initial guess of particle position:
	Parthasarathy, R. "Rapid, accurate particle tracking by
	calculation of radial symmetry centers." Nature Methods 9(7),
	724 - 726 (2012).

Maximum likelihood estimation of Gaussian spot with Poisson noise:
	Smith CS, Joseph N, Rieger B & Lidke KA. "Fast, single-molecule
	localization that achieves theoretically minimum uncertainty."
	Nature Methods 7(5), 373 - 375 (2010).

Theoretical best-fitting Gaussian for a given NA and wavelength:
	Zhang B, Josiane Z & Olivo-Marin JC. "Gaussian approximations
	of fluorescence microscope point-spread function models."
	Applied Optics 46(10), 1819 - 1829 (2007).

Hungarian algorithm for log-likelihood-based trajectory reconnection:
	Kuhn HW. ``The Hungarian Method for the assignment problem.''
	Naval Research Logistics Quarterly 3, 253 - 258 (1956).

### Localization method ###

The core of the subpixel localization module relies on maximum likelihood estimation of the center, intensity, and background of a Gaussian model in the presence of Poisson noise, and derives from Smith CS et al. Nature Methods (2010). The following steps are performed sequentially:

1. Identify spot positions using a generalized log-likelihood ratio test (Arnauld et al. Nature Methods 2008).

2. For each spot, obtain a fast initial guess for the position using the method of maximum radial symmetry (Parthasarathy R. et al. Nature Methods 2012). In many cases, this guess is very good and is refined only slightly over the next few steps.

3. Use several rounds of Newton-Raphson with an MLE model for a Gaussian spot in the presence of Poisson noise to refine the position estimate. The algorithm uses a stabilized version of the model Hessian, which improves convergence. For most localizations, convergence is rapid, reaching sub-nm levels of refinement within ten rounds. 

4. Occasionally, the algorithm will fail to converge. This nearly always occurs when two localizations are very close. In these cases, the algorithm defaults to a greedy Gauss-Newton least-squares algorithm which converges to one of the two spots.

5. The localizations are recorded in a `locs.txt` file.

### Tracking method ###

In the present implementation, the tracking step is performed by generating a semigraph between existing trajectories and new localizations in each frame - that is, a matrix with x-indices corresponding to individual trajectories and y-indices corresponding to individual localizations. A given element is nonzero if the corresponding localization lies within the maximum search radius of the corresponding trajectory. The goal of tracking is to permute the semigraph until the assignment of trajectories to localizations maximizes a likelihood function.

To reduce the complexity of the problem, semigraphs for each frame-frame comparison are split into independent sets of connected components. Arrows in the semigraph of each connected component are then weighted according to a model of diffusion that incorporates that past history of each particle, and the Hungarian algorithm is then used to assign trajectories to localizations for each subgraph.
